from django.shortcuts import render, redirect
from .models import Project


def list_projects(request):
    projects = Project.objects.all()
    if projects:
        return render(request, 'projects/project_list.html',
                      {'projects': projects})
    else:
        return redirect("/projects/")
